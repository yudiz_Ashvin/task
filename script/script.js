// singup form

const signUp = e => {
	let fname = document.getElementById('fname').value,
		email = document.getElementById('email').value,
		pwd = document.getElementById('pwd').value;
		mobile = document.getElementById('mobile').value;
	   dob = document.getElementById('dob').value;
        const id = Math.floor(Math.random()*2000);
	let formData = JSON.parse(localStorage.getItem('formData')) || [];
  
	let exist = formData.length &&
	  JSON.parse(localStorage.getItem('formData')).some(data =>
		data.fname.toLowerCase() == fname.toLowerCase() 
	  );
  
	if (!exist) {
	  formData.push({ fname, email, pwd, mobiles: mobile, dob: dob ,Id:id});
	  localStorage.setItem('formData', JSON.stringify(formData));
	  document.querySelector('form').reset();
	  document.getElementById('fname').focus();
	  alert("Account Created.\n\nPlease Sign In using the link below.");
	}
	else {
	  alert("Ooopppssss... Duplicate found!!!\nYou have already sigjned up");
	}
	e.preventDefault();
  }

// for sign in

function signIn(e) {
	let userinfo = [];
	let email = document.getElementById('email').value, pwd = document.getElementById('pwd').value;
	let formData = JSON.parse(localStorage.getItem('formData'))||[];
  
	let data = formData.find(user => user.email === email);
	  userinfo.push(data);
	 localStorage.setItem('userlogin',JSON.stringify(userinfo));
	
	let exist = formData.length &&
	  formData.some(data => data.email.toLowerCase() == email && data.pwd.toLowerCase() == pwd);
	if (!exist) {
	  alert("Incorrect login credentials");
	}
	else {
	  location.href = "../html/index.html";
	}
	e.preventDefault();
  }
  
// todo list

window.addEventListener('load', (e) => {
	const form = document.querySelector("#new-task-form");
	const input = document.querySelector("#new-task-input");
	const list_el = document.querySelector("#tasks");

	form.addEventListener('submit', (e) => {
		e.preventDefault();

		const task = input.value;

		const task_el = document.createElement('div');
		task_el.classList.add('task');

		const task_content_el = document.createElement('div');
		task_content_el.classList.add('content');

		task_el.appendChild(task_content_el);

		const task_input_el = document.createElement('input');
		task_input_el.classList.add('text');
		task_input_el.type = 'text';
		task_input_el.value = task;
		task_input_el.setAttribute('readonly', 'readonly');
	

		task_content_el.appendChild(task_input_el);

		const task_actions_el = document.createElement('div');
		task_actions_el.classList.add('actions');

		
		const task_edit_el = document.createElement('button');
		task_edit_el.classList.add('edit');
		task_edit_el.innerText = 'Edit';

		const task_delete_el = document.createElement('button');
		task_delete_el.classList.add('delete');
		task_delete_el.innerText = 'Delete';

		task_actions_el.appendChild(task_edit_el);
		task_actions_el.appendChild(task_delete_el);

		task_el.appendChild(task_actions_el);

		list_el.appendChild(task_el);

		input.value = '';

		if(task_input_el.value == ''){
			alert('empty task are not allow')
		}

		task_edit_el.addEventListener('click', () => {
			if (task_edit_el.innerText.toLowerCase() == "edit") {
				task_edit_el.innerText = "Save";
				task_input_el.removeAttribute("readonly");
				task_input_el.focus();
			} else {
				task_edit_el.innerText = "Edit";
				task_input_el.setAttribute("readonly", "readonly");
			}
		});

		task_delete_el.addEventListener('click', (e) => {
			list_el.removeChild(task_el);
		});
	});
});

// for date and time

function display_c(){
  var refresh=1000; // Refresh rate in milli seconds
  mytime=setTimeout('display_ct()',refresh)
}

function display_ct() {
 var CDate = new Date()
 var NewDate=CDate.toDateString(); 
 NewDate = NewDate + " - " + CDate.toLocaleTimeString();
 document.getElementById('ct').innerHTML = NewDate;
 display_c();
}

function clearData() {
	localStorage.removeItem('fname');
	localStorage.removeItem('email');
	window.location.href = '../html/Login.html';
}

// for image 
function image() {
    fetch(`https://api.giphy.com/v1/gifs/random?api_key=w5kVyG3lzcm2yQNbIHsIImohiaZCklHH`)
        .then((response) => response.json())
        .then((data) => displayGif(data.data.images.downsized_medium.url));
}
function displayGif(url) {
    const gif2 = document.querySelector(".img");
    gif2.innerHTML = `<img src="${url}" style="width: 100px;" alt="gif">`;
}
image();
setInterval(() => {
    image();
}, 20000);


//For Weather

function getWeather() {
	let temperature = document.getElementById("temperature");
	let description = document.getElementById("description");
	let location = document.getElementById("location");
  
	let api = "https://api.openweathermap.org/data/2.5/weather";
	let apiKey = "f146799a557e8ab658304c1b30cc3cfd";
  
	location.innerHTML = "Locating...";
  
	navigator.geolocation.getCurrentPosition(success, error);
  
	function success(position) {
	  latitude = position.coords.latitude;
	  longitude = position.coords.longitude;
  
	  let url = api + "?lat=" + latitude + "&lon=" + longitude + "&appid="+ apiKey + "&units=imperial";
  
	  fetch(url)
		.then((response) => response.json())
		.then((data) => {
		  console.log(data);
		  let temp = data.main.temp;
		  temperature.innerHTML = temp + "° F";
		  location.innerHTML =
			data.name + " (" + latitude + "°, " + longitude + "°)";
		  description.innerHTML = data.weather[0].main;
		});
	}
  
	function error() {
	  location.innerHTML = "Unable to retrieve your location";
	}
  }
  getWeather();
